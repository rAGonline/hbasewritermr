//package com.hbasewrite;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

//import org.apache.directory.api.util.exception.Exceptions;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileSplit;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.util.Tool;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class HbasewriteMapper extends Mapper <LongWritable, Text, LongWritable, Text> implements Tool{
	List<String> contents = new ArrayList();
	static BufferedReader br;
	HTable table ;
	
	protected void setup(Context context) throws IOException, InterruptedException {
	/*	try{
			Path path = ((FileSplit) context.getInputSplit()).getPath();
			String filepath = path.toString();
			String filename = path.getName();
			
			br = new BufferedReader(new FileReader(filepath+"/"+filename));
		//	while((br.readLine()!=null)){
				
		//	}
			
			
			
		}catch(Exception e){
			e.printStackTrace();
		}*/
		
		table = new HTable(context.getConfiguration(), "BookBigT1");
		System.out.println("comes here");
		
	}
	
	protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		String line = value.toString();
		String[] tokens = line.split(",");
		
		//tokens[0] will have the ISBN
		Put p = new Put(Bytes.toBytes(tokens[0]));
		p.add(Bytes.toBytes("name"),Bytes.toBytes("BookTitle"),Bytes.toBytes(tokens[1]));
		p.add(Bytes.toBytes("name"),Bytes.toBytes("author"),Bytes.toBytes(tokens[2]));
		p.add(Bytes.toBytes("publish"),Bytes.toBytes("YrOfPublication"),Bytes.toBytes(tokens[3]));
		p.add(Bytes.toBytes("publish"),Bytes.toBytes("Publisher"),Bytes.toBytes(tokens[4]));
		p.add(Bytes.toBytes("picture"),Bytes.toBytes("image-url"),Bytes.toBytes(tokens[5]));
		System.out.println(p.toString());
		// Saving the put Instance to the HTable.
		table.put(p);
		System.out.println("data Updated");

		// closing HTable
		table.close();
		
		
		
	}

	@Override
	public Configuration getConf() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setConf(Configuration arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int run(String[] arg0) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

}
