//package com.hbasewrite;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Scan;
//import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileSplit;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


//import com.hbasewrite.Hbasewritedriver;
//import com.hbasewrite.HbasewriteMapper;

public class Hbasewritedriver extends Configured implements Tool {
	
	public static void main(String args[]) throws Exception{
		int returnStatus = ToolRunner.run(new Hbasewritedriver(), args);
		System.exit(returnStatus);
	}

	public int run(String args[]) throws Exception{
		
		//System.out.println(args.length);
		// Make sure there are exactly 2 parameters
		if (args.length != 2) {
		throw new IllegalArgumentException("Usage: Hbasewritedriver" + " <input-path> <output-path>");
		}
		
		Configuration conf = getConf(); 
		//Job job = new Job(conf,"Secondary Sort");
		Job job = Job.getInstance();
		
		//config = HBaseConfiguration.create();
		//Job job = new Job(config,"Hbasewritedriver");
		//job.getConfiguration();
		job.setJarByClass(Hbasewritedriver.class);     // class that contains mapper and reducer
		job.setJobName("Hbasewritedriver");

		Path inputPath = new Path(args[0]);
		Path outputPath = new Path(args[1]);
		
		job.setMapperClass(HbasewriteMapper.class);
		//Add input and output file paths to job based on the arguments passed
		FileInputFormat.setInputPaths(job, inputPath);
		FileOutputFormat.setOutputPath(job, outputPath);
		
		/*Scan scan = new Scan();
		scan.setCaching(500);        // 1 is the default in Scan, which will be bad for MapReduce jobs
		scan.setCacheBlocks(false);  // don't set to true for MR jobs
		// set other scan attrs

		TableMapReduceUtil.initTableMapperJob(
			sourceTable,        // input table
			scan,               // Scan instance to control CF and attribute selection
			HbasewriteMapper.class,     // mapper class
			Text.class,         // mapper output key
			IntWritable.class,  // mapper output value
			job);
		TableMapReduceUtil.initTableReducerJob(
			targetTable,        // output table
			MyTableReducer.class,    // reducer class
			job);
		job.setNumReduceTasks(1);   // at least one, adjust as required
*/		
		//Wait for the job to complete and print if the job was successful or not
		int returnValue = job.waitForCompletion(true) ? 0:1;
		
		if(job.isSuccessful()) {
			System.out.println("Job was successful");
			} else if(!job.isSuccessful()) {
				System.out.println("Job was not successful");          
			}
		return returnValue;

		}

}
